package Vista;

import Util.seed.Pila;
import java.util.Scanner;
/**
 *
 * @author Angel Diaz Vanegas 1152242
 */
public class TestValidador_Agrupamiento {
    public static void main(String[] args) {
        System.out.println("Validando [{()}] ");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        boolean isValid = validateString(input);
        System.out.println("Input: " + input);
        System.out.println(isValid ? "V" : "F");
    }

    public static boolean validateString(String input) {
        Pila<Character> pila = new Pila<>();
        Pila<Character> parentesisPila = new Pila<>();

        for (char c : input.toCharArray()) {
            if (isOpenGrouping(c)) {
                if (!parentesisPila.isVacia() && parentesisPilaTope(parentesisPila) == '(') {
                    // Existe un paréntesis abierto, no se permite otro signo de apertura dentro de él
                    return false;
                }
                pila.push(c);
            } else if (isCloseGrouping(c)) {
                if (pila.isVacia() || !isMatchingGrouping(pila, c)) {
                    return false;
                }
                if (!parentesisPila.isVacia() && parentesisPilaTope(parentesisPila) == '(') {
                    // Cerró un paréntesis, se puede permitir otro signo de apertura dentro
                    parentesisPila.pop();
                }
            }
            if (c == '(') {
                // Se encontró un paréntesis abierto, se guarda en la pila de paréntesis
                parentesisPila.push(c);
            }
        }

        return pila.isVacia();
    }

    public static boolean isOpenGrouping(char c) {
        return c == '{' || c == '[' || c == '(';
    }

    public static boolean isCloseGrouping(char c) {
        return c == '}' || c == ']' || c == ')';
    }

    public static boolean isMatchingGrouping(Pila<Character> pila, char close) {
        char open = getMatchingOpeningGrouping(close);
        if (open == '\0') {
            return false;
        }

        // Verificar si hay signos de apertura intermedios y validar su jerarquía
        Pila<Character> tempPila = new Pila<>();
        boolean foundMatch = false;

        while (!pila.isVacia()) {
            char top = pila.pop();
            if (top == open) {
                foundMatch = true;
                break;
            } else {
                tempPila.push(top);
            }
        }

        while (!tempPila.isVacia()) {
            pila.push(tempPila.pop());
        }

        return foundMatch;
    }

    public static char getMatchingOpeningGrouping(char close) {
        switch (close) {
            case '}':
                return '{';
            case ']':
                return '[';
            case ')':
                return '(';
            default:
                return '\0';
        }
    }
    
    public static char parentesisPilaTope(Pila<Character> pila) {
        Pila<Character> tempPila = new Pila<>();
        char tope = '\0';
        
        while (!pila.isVacia()) {
            char c = pila.pop();
            tempPila.push(c);
            tope = c;
        }
        
        while (!tempPila.isVacia()) {
            pila.push(tempPila.pop());
        }
        
        return tope;
    }
}