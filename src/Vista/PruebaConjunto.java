package Vista;

import Util.seed.Conjunto;

public class PruebaConjunto {
    public static void main(String[] args) {
        Conjunto<Integer> conjuntoA = new Conjunto<>();
        conjuntoA.insertar(1);
        conjuntoA.insertar(2);
        conjuntoA.insertar(3);
        conjuntoA.insertar(4);
        conjuntoA.insertar(5);
        conjuntoA.insertar(8);
        conjuntoA.insertar(9);

        Conjunto<Integer> conjuntoB = new Conjunto<>();
        conjuntoB.insertar(2);
        conjuntoB.insertar(3);
        conjuntoB.insertar(4);
        conjuntoB.insertar(1);
        conjuntoB.insertar(5);
        conjuntoB.insertar(6);
        conjuntoB.insertar(7);

        
        System.out.println("Conjunto A: " + conjuntoA);
        System.out.println("Conjunto B: " + conjuntoB);
        
        Conjunto<Integer> union = conjuntoA.getUnion(conjuntoB);
        System.out.println("Unión: " + union);
        
        Conjunto<Integer> interseccion = conjuntoA.getInterseccion(conjuntoB);
        System.out.println("Intersección: " + interseccion);
        
        Conjunto<Integer> diferencia = conjuntoA.getDiferencia(conjuntoB);
        System.out.println("Diferencia: " + diferencia);
        
        Conjunto<Integer> diferenciaAsimetrica = conjuntoA.getDiferenciaAsimetrica(conjuntoB);
        System.out.println("Diferencia Asimétrica: " + diferenciaAsimetrica);
        
        Conjunto<Conjunto<Integer>> pares = conjuntoA.getPares();
        System.out.println("Pares: " + pares);
        
        Conjunto<Conjunto<Integer>> potenciaA = conjuntoA.getPotencia();
        System.out.println("Potencia de A: " + potenciaA);
        
        Conjunto<Conjunto<Integer>> potenciaB = conjuntoB.getPotencia();
        System.out.println("Potencia de B: " + potenciaB);
    }
}