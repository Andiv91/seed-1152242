/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Util.seed;

/**
 * Wrapper (Envoltorio)
 *
 * @author Angel Diaz Vanegas
 */
public class Conjunto<T> implements IConjunto<T> {

    private ListaS<T> elementos;

    public Conjunto() {
        elementos = new ListaS<>();
    }

    @Override
    public Conjunto<T> getUnion(Conjunto<T> c1) {
        if (this.elementos.isVacia() && c1.elementos.isVacia()) {
            throw new RuntimeException("Ambos conjuntos están vacíos.");
        }
        Conjunto<T> union = new Conjunto<>();

        // Agregar los elementos del primer conjunto
        for (int i = 0; i < this.elementos.getSize(); i++) {
            union.insertar(this.elementos.get(i));
        }

        // Agregar los elementos del segundo conjunto que no estén en el primero
        for (int i = 0; i < c1.elementos.getSize(); i++) {
            T elemento = c1.elementos.get(i);
            if (!union.elementos.contains(elemento)) {
                union.insertar(elemento);
            }
        }

        return union;
    }

    @Override
    public Conjunto<T> getInterseccion(Conjunto<T> c1) {
        Conjunto<T> interseccion = new Conjunto<>();

        for (int i = 0; i < this.elementos.getSize(); i++) {
            T elemento = this.elementos.get(i);
            if (c1.elementos.contains(elemento)) {
                interseccion.insertar(elemento);
            }
        }

        return interseccion;
    }

    @Override
    public Conjunto<T> getDiferencia(Conjunto<T> c1) {
        if (this.elementos.isVacia() || c1.elementos.isVacia()) {
            throw new RuntimeException("Uno de los conjuntos está vacío.");
        }

        Conjunto<T> diferencia = new Conjunto<>();

        for (int i = 0; i < this.elementos.getSize(); i++) {
            T elemento = this.elementos.get(i);
            if (!c1.elementos.contains(elemento)) {
                diferencia.insertar(elemento);
            }
        }

        return diferencia;
    }

    @Override
    public Conjunto<T> getDiferenciaAsimetrica(Conjunto<T> c1) {
        if (this.elementos.isVacia() || c1.elementos.isVacia()) {
            throw new RuntimeException("Uno de los conjuntos está vacío.");
        } else if (this.elementos.isVacia()) {
            return c1;
        } else if (c1.elementos.isVacia()) {
            return this;
        }

        Conjunto<T> diferenciaAsimetrica = this.getDiferencia(c1);
        diferenciaAsimetrica.elementos.addAll(c1.getDiferencia(this).elementos);
        return diferenciaAsimetrica;
    }

    public Conjunto<Conjunto<T>> getPares() {
        Conjunto<Conjunto<T>> pares = new Conjunto<>();

        for (int i = 0; i < elementos.getSize(); i++) {
            for (int j = i + 1; j < elementos.getSize(); j++) {
                Conjunto<T> par = new Conjunto<>();
                par.insertar(elementos.get(i));
                par.insertar(elementos.get(j));
                pares.insertar(par);
            }
        }

        return pares;
    }

    @Override
    public T get(int i) {
        return elementos.get(i);
    }

    @Override
    public void set(int i, T info) {
        elementos.set(i, info);
    }

    @Override
    public void insertar(T info) {
        if (elementos.contains(info)) {
            throw new RuntimeException("El elemento ya está presente en el conjunto.");
        }
        elementos.insertarInicio(info);
    }

    @Override
    public Conjunto<Conjunto<T>> getPotencia() {
        if (elementos.isVacia()) {
            throw new RuntimeException("El conjunto original está vacío.");
        }

        Conjunto<Conjunto<T>> potencia = new Conjunto<>();
        Conjunto<T> conjuntoVacio = new Conjunto<>();
        potencia.insertar(conjuntoVacio); // Agregar el conjunto vacío

        // Obtener los elementos del conjunto actual
        ListaS<T> elementosActuales = this.elementos;

        // Generar subconjuntos para cada elemento
        generarSubconjuntos(elementosActuales, conjuntoVacio, potencia);

        return potencia;
    }

    private void generarSubconjuntos(ListaS<T> elementosRestantes, Conjunto<T> subconjuntoActual, Conjunto<Conjunto<T>> potencia) {
        if (!elementosRestantes.isVacia()) {
            T primerElemento = elementosRestantes.get(0);

            // Generar subconjuntos sin el primer elemento
            ListaS<T> elementosRestantesSinPrimero = new ListaS<>();
            for (int i = 1; i < elementosRestantes.getSize(); i++) {
                elementosRestantesSinPrimero.insertarFin(elementosRestantes.get(i));
            }
            generarSubconjuntos(elementosRestantesSinPrimero, subconjuntoActual, potencia);

            // Generar subconjuntos con el primer elemento
            Conjunto<T> nuevoSubconjunto = new Conjunto<>();
            nuevoSubconjunto.insertar(primerElemento);
            nuevoSubconjunto = nuevoSubconjunto.getUnion(subconjuntoActual);
            potencia.insertar(nuevoSubconjunto);

            // Generar subconjuntos con el primer elemento y los restantes
            generarSubconjuntos(elementosRestantesSinPrimero, nuevoSubconjunto, potencia);
        }
    }

    @Override
    public String toString() {
        if (elementos.isVacia()) {
            return "{}";
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("{");
            for (int i = 0; i < elementos.getSize(); i++) {
                sb.append(elementos.get(i));
                if (i != elementos.getSize() - 1) {
                    sb.append(", ");
                }
            }
            sb.append("}");
            return sb.toString();
        }
    }
}
